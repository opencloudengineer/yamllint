# [1.1.0](https://gitlab.com/opencloudengineer/yamllint/compare/v1.0.0...v1.1.0) (2020-11-06)


### Features

* yamllint as a hidden job ([3cd6527](https://gitlab.com/opencloudengineer/yamllint/commit/3cd6527f6949e73837c1066d6192950c325d77e7))

# 1.0.0 (2020-09-07)


### Features

* add ci for yaml:lint job ([c9b92cb](https://gitlab.com/opencloudengineer/yamllint/commit/c9b92cbbfb25ac134872c31a08ce9e522327082b))
* add rlz ci ([7a69a74](https://gitlab.com/opencloudengineer/yamllint/commit/7a69a74f422b9e2af02ba7a13c4305d923fc81d8))
* more recursion ([635bfff](https://gitlab.com/opencloudengineer/yamllint/commit/635bfff395027e2ac83e267654ad37e775c53b88))
